######################################################################
# Qt Project for calculator
######################################################################

QT += core gui widgets
CONFIG += c++11

TEMPLATE = app
TARGET = calculator
INCLUDEPATH += .

# Input
HEADERS += calculator.h
SOURCES += main.cpp calculator.cpp
