// Author: Alex Gittemeier <asg8w9@mst.edu>
// Date: 2017-04-30

#include <QVBoxLayout>
#include <QGridLayout>
#include <QKeyEvent>
#include "calculator.h"

QPushButton *style_button(QPushButton *btn);

Calculator::Calculator() {
	QWidget *main = new QWidget;
	QVBoxLayout *main_layout = new QVBoxLayout;
	main->setLayout(main_layout);
	setCentralWidget(main);
	setMaximumSize(400, 500);
	installEventFilter(this);

	display = new QLineEdit;
	QFont display_font;
	display_font.setPointSize(24);
	display->setFont(display_font);
	display->setReadOnly(true);
	display->setAlignment(Qt::AlignRight);
	main_layout->addWidget(display);

	QWidget *buttons = new QWidget;
	QGridLayout *buttons_layout = new QGridLayout;
	buttons_layout->setSpacing(0);
	buttons->setLayout(buttons_layout);
	main_layout->addWidget(buttons);

	for (int i = 0; i < 10; i++)
		btn_digits[i] = style_button(new QPushButton(QString::number(i), this));
	btn_clear_entry = style_button(new QPushButton("CE", this));
	btn_clear_all = style_button(new QPushButton("AC", this));
	btn_backspace = style_button(new QPushButton("\u232B", this));
	btn_divide = style_button(new QPushButton("/", this));
	btn_multiply = style_button(new QPushButton("*", this));
	btn_subtract = style_button(new QPushButton("-", this));
	btn_add = style_button(new QPushButton("+", this));
	btn_change_sign = style_button(new QPushButton("\u00B1", this));
	btn_decimal = style_button(new QPushButton(".", this));
	btn_equals = style_button(new QPushButton("=", this));

	auto signal = &QPushButton::clicked;
	for (int i = 0; i < 10; i++)
		connect(btn_digits[i], signal, [this, i](){ pressed_digit(i); });
	connect(btn_clear_entry, signal, this, &Calculator::pressed_clear_entry);
	connect(btn_clear_all, signal, this, &Calculator::pressed_clear_all);
	connect(btn_backspace, signal, this, &Calculator::pressed_backspace);
	connect(btn_divide, signal, [this](){ pressed_op('/'); });
	connect(btn_multiply, signal, [this](){ pressed_op('*'); });
	connect(btn_subtract, signal, [this](){ pressed_op('-'); });
	connect(btn_add, signal, [this](){ pressed_op('+'); });
	connect(btn_change_sign, signal, this, &Calculator::pressed_change_sign);
	connect(btn_decimal, signal, this, &Calculator::pressed_decimal);
	connect(btn_equals, signal, this, &Calculator::pressed_equals);

	buttons_layout->addWidget(btn_clear_entry, 0, 0);
	buttons_layout->addWidget(btn_clear_all, 0, 1);
	buttons_layout->addWidget(btn_backspace, 0, 2);
	buttons_layout->addWidget(btn_divide, 0, 3);
	buttons_layout->addWidget(btn_digits[7], 1, 0);
	buttons_layout->addWidget(btn_digits[8], 1, 1);
	buttons_layout->addWidget(btn_digits[9], 1, 2);
	buttons_layout->addWidget(btn_multiply, 1, 3);
	buttons_layout->addWidget(btn_digits[4], 2, 0);
	buttons_layout->addWidget(btn_digits[5], 2, 1);
	buttons_layout->addWidget(btn_digits[6], 2, 2);
	buttons_layout->addWidget(btn_subtract, 2, 3);
	buttons_layout->addWidget(btn_digits[1], 3, 0);
	buttons_layout->addWidget(btn_digits[2], 3, 1);
	buttons_layout->addWidget(btn_digits[3], 3, 2);
	buttons_layout->addWidget(btn_add, 3, 3);
	buttons_layout->addWidget(btn_change_sign, 4, 0);
	buttons_layout->addWidget(btn_digits[0], 4, 1);
	buttons_layout->addWidget(btn_decimal, 4, 2);
	buttons_layout->addWidget(btn_equals, 4, 3);

	update_display();
}

QPushButton *style_button(QPushButton *btn) {
	QFont btn_font;
	btn_font.setPointSize(18);
	btn->setFont(btn_font);
	btn->setFlat(true);
	btn->setMinimumSize(80, 50);
	btn->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	return btn;
}

// =======
//  Slots
// =======

void Calculator::update_display() {
	if (show_lhs)
		display->setText(QString::number(lhs));
	else if (input_negative)
		display->setText("-" + input);
	else
		display->setText(input);
}

double Calculator::get_input() const {
	return input_negative ? -input.toDouble() : input.toDouble();
}

void Calculator::clear_input() {
	input = "0";
	input_negative = false;
}

void Calculator::pressed_digit(int digit) {
	if (input == "0")
		input = QString::number(digit);
	else
		input += QString::number(digit);
	show_lhs = false;
	update_display();
}

void Calculator::pressed_clear_entry() {
	clear_input();
	show_lhs = true;
	update_display();
}

void Calculator::pressed_clear_all() {
	input_negative = false;
	input = "0";
	lhs = 0;
	op = 0;
	rhs = 0;
	show_lhs = false;
	op_equals = false;
	update_display();
}

void Calculator::pressed_backspace() {
	input.remove(input.size() - 1, 1);
	if (input.isEmpty())
		input = "0";
	show_lhs = false;
	update_display();
}

void Calculator::pressed_op(char op) {
	if (this->op == 0 || op_equals) {
		if (show_lhs)
			rhs = lhs;
		else 
			rhs = lhs = get_input();
		clear_input();
		show_lhs = true;
	}
	else if (!show_lhs)
		pressed_equals();

	this->op = op;
	op_equals = false;
	update_display();
}

void Calculator::pressed_change_sign() {
	input_negative = !input_negative;
	show_lhs = false;
	update_display();
}

void Calculator::pressed_decimal() {
	if (!input.contains('.'))
		input += '.';
	show_lhs = false;
	update_display();
}

void Calculator::pressed_equals() {
	if (!show_lhs) {
		rhs = get_input();
		clear_input();
	}

	switch(op) {
	case '+':
		lhs += rhs;
		break;
	case '-':
		lhs -= rhs;
		break;
	case '/':
		lhs /= rhs;
		break;
	case '*':
		lhs *= rhs;
		break;
	default:
		lhs = rhs;
	}
	show_lhs = true;
	op_equals = true;
	update_display();
}

// Boilerplate source: http://stackoverflow.com/q/11618664
// This function adopts the same keybindings as windows calculator
bool Calculator::eventFilter(QObject *o __attribute__((unused)), QEvent *e) {
	if (e->type() == QEvent::KeyPress) {
		int key = static_cast<QKeyEvent *>(e)->key();
		if (Qt::Key_0 <= key && key <= Qt::Key_9) {
			pressed_digit(key - Qt::Key_0);
			return true;
		}

		switch (key) {
		case Qt::Key_Delete:
			pressed_clear_entry();
			return true;
		case Qt::Key_Escape:
			pressed_clear_all();
			return true;
		case Qt::Key_Backspace:
			pressed_backspace();
			return true;
		case Qt::Key_Slash:
			pressed_op('/');
			return true;
		case Qt::Key_Asterisk:
			pressed_op('*');
			return true;
		case Qt::Key_Minus:
			pressed_op('-');
			return true;
		case Qt::Key_Plus:
			pressed_op('+');
			return true;
		case Qt::Key_Return:
		case Qt::Key_Enter:
		case Qt::Key_Equal:
			pressed_equals();
			return true;
		case Qt::Key_Period:
			pressed_decimal();
			return true;
		case Qt::Key_F9:
			pressed_change_sign();
			return true;
		}
	}
	return false;
}
