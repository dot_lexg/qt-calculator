# Project 2

Project options and details are available
[on the course website](http://dsl.mwisely.xyz/projects/2/).

For this project, I chose:

- [ ] Project A: Makefile Helpers
- [ ] Project B: C++ Standard Library
- [x] Project C: GUIs
- [ ] Project D: [Your title here]

To run this project, run the following commands:

    qmake
    make
    calculator    
