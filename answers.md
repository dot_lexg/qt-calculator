First thing I did was get my project set up, which involved choosing project
C in the readme, and adapting the Qt project file and main.cpp from lab 13 to
work with this project.

Next thing I did was I laid out my buttons and display, styled them correctly
and stubbed out slots for each of the buttons.

Then I added all of the state information I think the calculator needs, and
set off to implementing the behaviour of each button, one-by-one.

After everything seemed to work, I added a keyboard handler so that all keys
can be activated from the keyboard.
