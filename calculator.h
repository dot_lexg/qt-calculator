// Author: Alex Gittemeier <asg8w9@mst.edu>
// Date: 2017-04-30

#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>

class Calculator : public QMainWindow {
	Q_OBJECT

	QLineEdit *display;
	QPushButton *btn_digits[10];
	QPushButton *btn_clear_entry;
	QPushButton *btn_clear_all;
	QPushButton *btn_backspace;
	QPushButton *btn_divide;
	QPushButton *btn_multiply;
	QPushButton *btn_subtract;
	QPushButton *btn_add;
	QPushButton *btn_change_sign;
	QPushButton *btn_decimal;
	QPushButton *btn_equals;

	bool input_negative = false;
	QString input = "0";
	double lhs = 0;
	char op = 0;
	double rhs = 0;
	bool show_lhs = false;
	bool op_equals = false;


	void update_display();
	double get_input() const;
	void clear_input();

private slots:
	void pressed_digit(int digit);
	void pressed_clear_entry();
	void pressed_clear_all();
	void pressed_backspace();
	void pressed_op(char op);
	void pressed_change_sign();
	void pressed_decimal();
	void pressed_equals();

protected:
	bool eventFilter(QObject *o, QEvent *e);

public:
	Calculator();

};

#endif
