// Author: Alex Gittemeier <asg8w9@mst.edu>
// Derived from code committed by Michael Wisely
// 
// Date: 2017-04-30

#include <QApplication>
#include "calculator.h"

int main(int argc, char **argv) {
    QApplication app(argc, argv);
    Calculator main_class;
    main_class.show();

    return app.exec();
}
